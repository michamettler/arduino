#include <TimerOne.h>
#include <Wire.h>
#include <MultiFuncShield.h>

int randomNr = 0;
int eingabe = 0;

void setup() {
   // put your setup code here, to run once:
   Timer1.initialize();
   MFS.initialize(&Timer1); // initialize multi-function shield library
   MFS.write("rate");
   randomNr = rand() % 10 + 1;
   delay(2000);
   MFS.write(0);
}

void loop() {
  // put your main code here, to run repeatedly:
  byte btn = MFS.getButton();

  //Eingabe um 1 erhöhen
  if (btn == BUTTON_1_PRESSED){
    //Dafür sorgen, dass Eingabe nicht höher als 10 ist (da Zufallszahl zwischen 1 und 10 liegt)
    if(eingabe < 10){
      eingabe = eingabe + 1;
      MFS.write(eingabe);
    } else{
      eingabe = 0;
      MFS.write(eingabe);
    }
  }

  //Eingabe zurücksetzen
  if (btn == BUTTON_2_PRESSED){
    eingabe = 0;
    MFS.write(eingabe);
  }

  //Eingabe prüfen
  if (btn == BUTTON_3_PRESSED){
    if(eingabe == randomNr){
        MFS.beep(5, 5, 2);
        MFS.write("Nice");
        delay(2000);
        MFS.write("try");
        eingabe = 0;
      } else{
        MFS.writeLeds(LED_ALL, ON);
        MFS.write("NO");
        delay(2000);
        MFS.writeLeds(LED_ALL, OFF);
        MFS.write(eingabe);
      }
  }
}