#include <TimerOne.h>
#include <Wire.h>
#include <MultiFuncShield.h>

//Variablen deklarieren
int pinTrue = 1234;
int pin = 0;
int addition = 1;

void setup() {
   // put your setup code here, to run once:
   Timer1.initialize();
   MFS.initialize(&Timer1); // initialize multi-function shield library
   MFS.write(000);
   //LED 1 aktivieren um zu signalisieren, dass die erste Stelle der PIN-Anzeige mutiert wird
   MFS.writeLeds(LED_1, ON);
}

void loop() {
   // put your main code here, to run repeatedly:
   byte btn = MFS.getButton();

   //If-Clause um bei Betätigungen des ersten Buttons die Eingabe dem Wert der LED entsprechend zu addieren
   if (btn == BUTTON_1_PRESSED){
      //Abfangen einer zu hohen Eingabe
      if(pin+addition<=9999){
        pin = pin + addition;
        MFS.write(pin);
      } else {
        pin = 0;
        MFS.write(pin);
      }
   }

  //If-Clause um die jeweilige LED zu aktivieren, bzw. der addition-Variable mitzuteilen wieviel beim nächsten Mal zu addieren ist
   if (btn == BUTTON_2_PRESSED){
      if(addition==1){
        addition=10;
        MFS.writeLeds(LED_ALL, OFF);
        MFS.writeLeds(LED_2, ON);
      } else if(addition==10){
        addition=100;
        MFS.writeLeds(LED_ALL, OFF);
        MFS.writeLeds(LED_3, ON);
      } else if (addition==100){
        addition=1000;
        MFS.writeLeds(LED_ALL, OFF);
        MFS.writeLeds(LED_4, ON);
      } else{
        addition=1;
        MFS.writeLeds(LED_ALL, OFF);
        MFS.writeLeds(LED_1, ON);
      }
   }

   //Durch langes Betätigen des Button 3 überprüfen, ob Eingabe korrekt ist
   if (btn == BUTTON_3_LONG_PRESSED){
      if(pin == pinTrue){
        MFS.beep(5, 5, 2);
        MFS.write("YES");
        delay(2000);
        MFS.write(pin);
      } else{
        MFS.beep(10);
        MFS.writeLeds(LED_ALL, ON);
        MFS.write("NO");
        delay(2000);
        MFS.writeLeds(LED_ALL, OFF);
        pin=0;
        MFS.write(pin);
      }
   }

   //Eingabe zurücksetzem
   if (btn == BUTTON_1_LONG_PRESSED){
      pin=0;
      addition=1;
      MFS.writeLeds(LED_ALL, OFF);
        MFS.writeLeds(LED_1, ON);
      MFS.write(pin);
   }
}